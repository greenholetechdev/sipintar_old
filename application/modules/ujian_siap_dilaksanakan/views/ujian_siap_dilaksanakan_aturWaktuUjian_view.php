<div class="row-fluid">
 <!-- block -->
 <div class="block">
  <div class="navbar navbar-inner block-header">
   <div class="muted pull-left">Pilih Limit Waktu Ujian</div>
  </div>
  <div class="block-content collapse in">
   <div class="span12">
    <div class="table-toolbar">
     <div class="btn-group">
      <!--<a href="" onclick="ujian_siap_dilaksanakan_data.makeSoal(event)"><button class="btn btn-success">Buat Soal <i class="icon-plus icon-white"></i></button></a>-->
     </div>
    </div>

    <div class="message">

    </div>

    <div class="controls">
     <input type="hidden" id="ujian" class="" value="<?php echo $ujian ?>"/>
     <select class="span6 m-wrap required" name="list_waktu" error="waktu Limit Ujian"
             id="list_waktu">
      <option value="">Pilih Limit Waktu Ujian</option>
      <?php foreach ($list_waktu as $v_time) { ?>
       <option value="<?php echo $v_time['id'] ?>">
        <?php echo $v_time['time_limit'] . ' (Menit)' ?>
       </option>
      <?php } ?>
     </select>
     <button style="margin-top: -10px;" type="button" class="btn btn-primary" onclick="ujian_siap_dilaksanakan_data.execAturWatuUjian()">Proses</button>     
    </div>
   </div>
  </div>
 </div>
 <!-- /block -->
</div>