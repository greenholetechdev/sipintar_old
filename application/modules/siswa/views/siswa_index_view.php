<div class="row-fluid">
 <div class="card title-module">
  <div class="card-content">
   <i class="mdi mdi-arrow-left mdi-18px hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
   <i class="mdi mdi-arrow-right mdi-18px show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
   <a href="<?php echo base_url().$module ?>" class="title-content"><?php echo $title ?></a>
   <hr/>
  </div>
 </div>
</div>

<div class="row-fluid">
 <div class="card">
  <div class="card-content">
   <!-- block -->
   <div class="">
    <div class="table-toolbar">
     <div class="btn-group">
      <a href="#" onclick="siswa_data.importSiswa()">
       <button class="btn btn-success" id="" 
               onclick="">
        <i class="mdi mdi-file-excel-box mdi-18px"></i> 
        Import Data Siswa
       </button>
      </a> 
      <a style="margin-left: 12px;" href="<?php echo base_url() . $module . '/add' ?>"><button class="btn btn-primary">Tambah <i class="icon-plus icon-white"></i></button></a>
     </div>
     <div class="btn-group pull-right">
<!--      <button data-toggle="dropdown" class="btn dropdown-toggle">Tools <span class="caret"></span></button>
      <ul class="dropdown-menu">
       <li><a href="#">Save as PDF</a></li>
       <li><a href="#">Export to Excel</a></li>
      </ul>-->
     </div>
    </div>
    <br/>

    <div class="table-toolbar">
     <div class="btn-group pull-right">
      <input class="input-xlarge focused" id="search" type="text" value="" 
             placeholder="Pencarian" onkeyup="siswa_data.search(this, event)">
     </div>
    </div>

    <br/>
    <br/>

    <div class="message">

    </div>
    <div class="data_siswa">

     <div class="sticky-table sticky-headers sticky-ltr-cells">
      <table cellpadding="0" cellspacing="0" border="0" class="" id="example2">
       <thead>
        <tr class="sticky-row">
         <th>No</th>
         <th>Nama</th>
         <th>Nis</th>
         <th>Jurusan</th>
         <th>Password</th>
         <th>Status</th>
         <th>Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($data_siswa)) { ?>
         <?php $no = $this->uri->segment(3) + 1; ?>
         <?php foreach ($data_siswa as $value) { ?>
          <tr class="odd gradeX">
           <td><?php echo $no++ ?></td>
           <td><?php echo $value['nama'] ?></td>
           <td><?php echo $value['nis'] ?></td>
           <td><?php echo $value['jurusan'] ?></td>
           <td class="center"><?php echo $value['password'] ?></td>
           <?php
           if ($value['is_login']) {
            $back_color = 'style="background-color:#5bb75b"';
           } else {
            $back_color = 'style="background-color:#da4f49"';
           }
           ?>
           <td <?php echo $back_color ?> class="center"><?php echo $value['is_login'] == true ? 'Sedang Login' : 'Belum Login' ?></td>
           <td class="center">
            <a href="<?php echo base_url() . $module . '/edit/' . $value['id'] ?>">
             <i class="icon-edit"></i>
            </a>
            <i class="icon-trash" onclick="siswa_data.remove('<?php echo $value['id'] ?>')"></i>           
            <button id="" class="btn btn-warning" onclick="siswa_data.resetLogin('<?php echo $value['id'] ?>')">Reset Login</button>
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td colspan="6">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>
       </tbody>
      </table>
     </div>    

     <div class="paging text-right">
      <?php echo $pagination ?>
     </div>
    </div>        
   </div>
  </div>
 </div>
</div>