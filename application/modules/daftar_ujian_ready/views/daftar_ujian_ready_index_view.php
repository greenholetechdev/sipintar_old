<div class="row-fluid">
 <div class="card title-module">
  <div class="card-content">
   <i class="mdi mdi-arrow-left mdi-18px hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
   <i class="mdi mdi-arrow-right mdi-18px show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
   <a href="#" class="title-content"><?php echo $title ?></a>
   <hr/>
  </div>
 </div>
</div>

<div class="row-fluid">
 <div class="card">
  <div class="card-content">
<div class="">
    <div class="table-toolbar">
     <div class="btn-group">
      <!--<a href="<?php echo base_url() . $module . '/add' ?>"><button class="btn btn-success">Tambah <i class="icon-plus icon-white"></i></button></a>-->
     </div>
     <div class="btn-group pull-right">
      <!--<button data-toggle="dropdown" class="btn dropdown-toggle">Tools <span class="caret"></span></button>-->
      <!--<ul class="dropdown-menu">-->
      <!--<li><a href="#">Save as PDF</a></li>-->
      <!--<li><a href="#">Export to Excel</a></li>-->
      <!--</ul>-->
     </div>
    </div>
    <br/>

    <div class="table-toolbar">
     <div class="btn-group pull-right">
      <input class="input-xlarge focused" id="search" type="text" value="" 
             placeholder="Pencarian" onkeyup="daftar_ujian_ready_data.search(this, event)">
     </div>
    </div>

    <br/>
    <br/>

    <div class="message">

    </div>
    <div class="data">
     <div class="sticky-table sticky-headers sticky-ltr-cells">
      <table cellpadding="0" cellspacing="0" border="0" class="" id="tabel_daftar_ujian_ready">
       <thead>
        <tr class="sticky-row">
         <th>No</th>
         <th>Kode Ujian</th>
         <th>Nama Ujian</th>
         <th>Mata Pelajaran</th>
         <th>Tanggal Ujian</th>
         <th>Waktu Ujian</th>
         <th>Total Soal</th>
         <th>Soal Sudah Dikerjakan</th>
         <th>Soal Belum Dikerjakan</th>
         <th>Lama Waktu Ujian</th>
         <th>Sisa Waktu</th>
         <th>Status</th>
         <th>Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($data_daftar_ujian_ready)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($data_daftar_ujian_ready as $value) { ?>
          <tr class="odd gradeX">
           <td><?php echo $no++ ?></td>
           <td><?php echo $value['kode_ujian'] ?></td>
           <td><?php echo $value['nama_ujian'] ?></td>
           <td><?php echo $value['mata_pelajaran'] ?></td>           
           <td><?php echo date('d M Y', strtotime($value['tanggal_ujian'])) ?></td>
           <td><?php echo $value['waktu_ujian'] ?></td>
           <td><?php echo $value['total_soal'] ?></td>
           <td><?php echo $value['total_dijawab'] ?></td>
           <td><?php echo $value['total_soal_belum'] ?></td>
           <td><?php echo $value['time_limit'].' Menit' ?></td>
           <td><?php echo $value['sisa_waktu'] == -1 ? '-' : $value['sisa_waktu'].' Menit' ?></td>
           <td><?php echo $value['is_submit'] == true ? 'Sudah Submit' : 'Belum Submit' ?></td>
           <td class="center">
            <?php if (!$value['is_submit']) { ?>
             <button id="" 
             <?php if ($value['total_dijawab'] != 0) { ?>
                      class="btn btn-warning" 
                     <?php } else { ?>
                      class="btn btn-success" 
                     <?php } ?>
                     onclick="daftar_ujian_ready_data.kerjakanUjian('<?php echo $value['id'] ?>', '<?php echo $value['ujian'] ?>', '<?php echo $value['token'] ?>')">
                      <?php
                      if ($value['total_dijawab'] != 0) {
                       echo 'Proses Pengerjaan';
                      } else {
                       echo 'Kerjakan';
                      }
                      ?>                
             </button>
            <?php }else{ ?>
            Selesai Dikerjakan
            <?php } ?>
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td colspan="6">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>
       </tbody>
      </table>
     </div>
    </div>        
   </div>
  </div>
 </div>
 <!-- block -->
<!-- <div class="block">
  <div class="navbar navbar-inner block-header">
   <div class="muted pull-left">Data <?php echo $title ?></div>
   <div class="pull-right"><span class="badge badge-info"><?php echo count($data_daftar_ujian_ready) ?></span>

   </div>
  </div>
  <div class="block-content collapse in">
   
  </div>
 </div>-->
 <!-- /block -->
</div>