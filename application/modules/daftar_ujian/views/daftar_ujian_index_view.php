<div class="row-fluid">
 <div class="navbar">
  <div class="navbar-inner">
   <ul class="breadcrumb">
    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
    <li>
     <a href="#" class="title-content"><?php echo $title ?></a> <span class="divider"></span>	
    </li>
   </ul>
  </div>
 </div>
</div>

<div class="row-fluid">
 <div class="card">
  <div class="card-content">
<div class="">
    <div class="table-toolbar">
    </div>
    <br/>

    <div class="table-toolbar">
     <div class="btn-group pull-right">
      <input class="input-xlarge focused" id="search" type="text" value="" 
             placeholder="Pencarian" onkeyup="daftar_ujian_data.search(this, event)">
     </div>
    </div>

    <br/>
    <br/>

    <div class="message">

    </div>
    <div class="data">
     <div class="sticky-table sticky-headers sticky-ltr-cells">
      <table cellpadding="0" cellspacing="0" border="0" class="" id="tabel_daftar_ujian">
       <thead>
        <tr class="sticky-row">
         <th>No</th>
         <th>Mata Pelajaran</th>
         <th>Kode Ujian</th>
         <th>Nama Ujian</th>
         <th>Tanggal Ujian</th>
         <th>Waktu Ujian</th>
         <th>Guru Pengajar</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($data_daftar_ujian)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($data_daftar_ujian as $value) { ?>
          <tr class="odd gradeX">
           <td><?php echo $no++ ?></td>
           <td><?php echo $value['mata_pelajaran'] ?></td>
           <td class="center"><?php echo $value['kode_ujian'] ?></td>
           <td class="center"><?php echo $value['nama_ujian'] ?></td>           
           <td class="center"><?php echo date('d M Y', strtotime($value['tanggal_ujian'])) ?></td>
           <td class="center"><?php echo $value['waktu_ujian'] ?></td>
           <td class="center"><?php echo $value['guru'] ?></td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td colspan="11">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>
       </tbody>
      </table>
     </div>     
    </div>        
   </div>
  </div>
 </div>
 <!-- block -->
<!-- <div class="block">
  <div class="navbar navbar-inner block-header">
   <div class="muted pull-left">Data <?php echo $title ?></div>
  </div>
  <div class="block-content collapse in">
   
  </div>
 </div>-->
 <!-- /block -->
</div>