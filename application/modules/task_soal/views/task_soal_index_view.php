<input type="hidden" id="ujian" class="" value="<?php echo $ujian ?>"/>
<div class="row-fluid">
 <div class="row-fluid">
 <div class="card title-module">
  <div class="card-content">
   <i class="mdi mdi-arrow-left mdi-18px hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
   <i class="mdi mdi-arrow-right mdi-18px show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
   <a href="#" class="title-content"><?php echo 'Soal' ?></a> <span class="divider"></span>	
   <hr/>
  </div>
 </div>
</div>
<!-- <div class="navbar">
  <div class="navbar-inner">
   <ul class="breadcrumb">
    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
    <li>
     <a href="#"><?php echo 'Soal' ?></a> <span class="divider"></span>	
    </li>
   </ul>
  </div>
 </div>-->
</div>

<div class="row-fluid">
 <!-- block -->
 <div class="block">
  <div class="navbar navbar-inner block-header">
   <div class="muted pull-left">Data <?php echo 'Soal yang Harus Dikerjakan' ?></div>
  </div>
  <div class="block-content collapse in">
   <br/>   
   <div class="message">

   </div>
   <div class="data">   
    <div class="span12">
     <?php if (!$siswa_is_submit && $is_exist_ujian && $is_start_ujian) { ?>
      <div id="rootwizard">
       <div class="navbar">
        <div class="navbar-inner">
         <div class="container">
          <ul>
           <?php $counter_kategori = 0; ?>
           <?php foreach ($data_soal_ujian as $v_jumlah) { ?>
            <?php $counter_kategori++ ?>
            <li><a href="#tab<?php echo $counter_kategori ?>" data-toggle="tab">Soal No. <?php echo $counter_kategori ?></a></li>
            <!--<li><a style="display: none !important;" href="#tab<?php echo $counter_kategori ?>" data-toggle="tab"></a></li>-->
           <?php } ?>
          </ul>
         </div>
        </div>
       </div>
       <!--       <div class="tab-content">
       <?php $counter_soal_per_kategori = 1; ?>
       <?php $no_soal = 1; ?>
       <?php foreach ($data_soal_ujian as $v_data_ujian) { ?>
        <?php $data_soal = $v_data_ujian['data_soal']; ?>
                  <div class="tab-pane" id="tab<?php echo $counter_soal_per_kategori++ ?>">         
        <?php foreach ($data_soal as $v_soal) { ?>
                      <div class="row-fluid soal">
                       <div class="span12">
                        <h5>Soal No. <?php echo $no_soal++ ?></h5>            
                       </div>
                       <div class="span12">
         <?php echo $v_soal['soal'] ?>&nbsp;&nbsp;&nbsp;
         <?php if ($v_soal['file_soal'] != '') { ?>
          <?php if ($v_soal['jenis_file'] != 'listening') { ?>
                              <iframe src="<?php echo base_url() . 'files/soal/' . $v_soal['file_soal'] ?>" width="200" height="200"></iframe>&nbsp;&nbsp;&nbsp;
          <?php } else { ?>
                              <button id="" class="btn btn-success" onclick="task_soal_data.playAudioListening(this,
                                        '<?php echo $v_data_ujian['ujian'] ?>', '<?php echo $v_soal['soal_id'] ?>')">Play Audio </button>
                              (<?php echo $v_soal['file_soal'] ?>)
                              <audio id="_audio">
                               <source src="<?php echo base_url() . 'files/soal/' . $v_soal['file_soal'] . '' ?>" type="audio/ogg">
                               Your browser does not support the audio element.
                              </audio>
                              <br/>
          <?php } ?>
         <?php } ?>
                       </div>
                       <div class="span12">
                        <div class="row-fluid">
                         <div class="span8">
                          <label class="" id="" style="color: #468847">Pilihan Jawaban : </label>&nbsp;
         <?php foreach ($v_soal['list_jawaban'] as $v_answer) { ?>
                             <input style="margin-top: -3px;" type="radio" id="" name="<?php echo $v_soal['id_soal'] ?>" 
                                    soal="<?php echo $v_soal['id_soal'] ?>" 
                                    class="radio" 
          <?php echo $v_answer['jawaban_siswa'] == true ? 'checked' : '' ?>
                                    jawaban="<?php echo $v_answer['id'] ?>" 
                                    is_true = "<?php echo $v_answer['true_or_false'] ?>"
                                    onclick="task_soal_data.answer(this)"/>
                             &nbsp;<?php echo $v_answer['jawaban'] == '' ? '-' : $v_answer['jawaban'] ?>&nbsp;&nbsp;&nbsp;
          <?php if ($v_answer['file_jawaban'] != '') { ?>
                                <iframe src="<?php echo base_url() . 'files/jawaban/' . $v_answer['file_jawaban'] ?>" width="200" height="200"></iframe>&nbsp;&nbsp;&nbsp;
          <?php } ?>
         <?php } ?>
                         </div>
                         <div class="4">
                          <button id="" class="btn btn-success" 
                                  onclick="task_soal_data.answeredSoal(this, 1)">Yakin</button>&nbsp;
                          <button id="" class="btn btn-warning"
                                  onclick="task_soal_data.answeredSoal(this, 2)">Ragu</button>
                         </div>
                        </div>
                       </div>
                      </div>         
                      <hr/>
        <?php } ?>
                  </div>
       <?php } ?>
               <ul class="pager wizard">
                <li class="previous first" style="display:none;"><a href="javascript:void(0);">First</a></li>
                <li class="previous"><a href="javascript:void(0);">Sebelumnya</a></li>
                <li class="next last" style="display:none;"><a href="javascript:void(0);">Terakhir</a></li>
                <li class="next"><a href="javascript:void(0);">Selanjutnya</a></li>
                <li class="next finish" style="display:none;"><a href="" onclick="task_soal_data.submitSoal(this, event)">Selesai</a></li>
               </ul>
              </div>  -->
       <div class="tab-content">
        <?php // $counter_soal_per_kategori = 1; ?>
        <?php $no_soal = 1; ?>
        <?php foreach ($data_soal_ujian as $v_data_ujian) { ?>
         <?php $data_soal = $data_soal_ujian; ?>
         <div class="tab-pane" id="tab<?php echo $no_soal ?>">         
          <?php foreach ($data_soal as $v_soal) { ?>
           <?php if ($v_soal['soal_id'] == $v_data_ujian['soal_id']) { ?>
            <div class="row-fluid soal">
             <div class="span12" style="margin-top: 12px;">
              <h5 style="font-size: 16px;font-weight: bold;">Soal No. <?php echo $no_soal++ ?></h5>
             </div>
             <div class="span12">
              <?php echo $v_soal['soal'] ?>&nbsp;&nbsp;&nbsp;
              <?php if ($v_soal['file_soal'] != '') { ?>
               <?php if ($v_soal['jenis_file'] != 'listening') { ?>
                <iframe src="<?php echo base_url() . 'files/soal/' . $v_soal['file_soal'] ?>" width="200" height="200"></iframe>&nbsp;&nbsp;&nbsp;
               <?php } else { ?>
                <button id="" class="btn btn-success" onclick="task_soal_data.playAudioListening(this,
                          '<?php echo $v_data_ujian['ujian'] ?>', '<?php echo $v_soal['soal_id'] ?>')">Play Audio </button>
                (<?php echo $v_soal['file_soal'] ?>)
                <audio id="_audio">
                 <source src="<?php echo base_url() . 'files/soal/' . $v_soal['file_soal'] . '' ?>" type="audio/ogg">
                 Your browser does not support the audio element.
                </audio>
                <br/>
               <?php } ?>
              <?php } ?>
             </div>
             <div class="span12">
              <div class="row-fluid">
               <div class="span8">
                <label class="" id="" style="color: #468847;font-size: 14px;">Pilihan Jawaban : </label>&nbsp;
                <?php foreach ($v_soal['list_jawaban'] as $v_answer) { ?>
                <label>
                 <input type="radio" id="" name="<?php echo $v_soal['soal_id'] ?>" 
                        soal="<?php echo $v_soal['soal_id'] ?>" 
                        class="radio" 
                        <?php echo $v_answer['jawaban_siswa'] == true ? 'checked' : '' ?>
                        jawaban="<?php echo $v_answer['id'] ?>" 
                        is_true = "<?php echo $v_answer['true_or_false'] ?>"
                        onclick="task_soal_data.answer(this)" />
                 <span><?php echo $v_answer['jawaban'] == '' ? '-' : $v_answer['jawaban'] ?>&nbsp;&nbsp;&nbsp;
                 <?php if ($v_answer['file_jawaban'] != '') { ?>
                  <iframe src="<?php echo base_url() . 'files/jawaban/' . $v_answer['file_jawaban'] ?>" width="200" height="200"></iframe>&nbsp;&nbsp;&nbsp;
                 <?php } ?>
                 </span>
                     </label>
<!--                 <input style="margin-top: -3px;" type="radio" id="" name="<?php echo $v_soal['soal_id'] ?>" 
                        soal="<?php echo $v_soal['soal_id'] ?>" 
                        class="radio" 
                        <?php echo $v_answer['jawaban_siswa'] == true ? 'checked' : '' ?>
                        jawaban="<?php echo $v_answer['id'] ?>" 
                        is_true = "<?php echo $v_answer['true_or_false'] ?>"
                        onclick="task_soal_data.answer(this)"/>-->
                <?php } ?>
               </div>
               <div class="4">
                <button id="" class="btn btn-success" 
                        onclick="task_soal_data.answeredSoal(this, 1)">Yakin</button>&nbsp;
                <button id="" class="btn btn-warning"
                        onclick="task_soal_data.answeredSoal(this, 2)">Ragu</button>
               </div>
              </div>
             </div>
            </div>         
            <hr/>
           <?php } ?>
          <?php } ?>
         </div>
        <?php } ?>
        <ul class="pager wizard">
         <li class="previous first" style="display:none;"><a href="javascript:void(0);">First</a></li>
         <li class="previous"><a href="javascript:void(0);">Sebelumnya</a></li>
         <li class="next last" style="display:none;"><a href="javascript:void(0);">Terakhir</a></li>
         <li class="next"><a href="javascript:void(0);">Selanjutnya</a></li>
         <li class="next finish" style="display:none;"><a href="" onclick="task_soal_data.submitSoal(this, event)">Selesai</a></li>
        </ul>
       </div>  
      </div>
     <?php } else { ?>
      <div class="">
       <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4>Success</h4>
        Belum Ada Ujian Diadakan
       </div>
      </div>
     <?php } ?>
    </div>
   </div>        
  </div>
 </div>
 <!-- /block -->
</div>