<div class="row-fluid">
 <input type='hidden' name='' id='ujian' class='form-control' value='<?php echo $ujian ?>'/>
 <!-- block -->
 <div class='block'>
  <div class="navbar navbar-inner block-header">
   <div class="muted pull-left">Form Kategori Soal 
   </div>
  </div>
  <div class="block-content collapse in">
   <form class="form-horizontal" style="">
    <div class='span12'>
     <div class="message">

     </div>
     <input type="hidden" id="id_kategori" class="" value="<?php echo isset($id_kategori) ? $id_kategori : '' ?>"/>
     <input type="hidden" id="mata_pelajaran" class="" value="<?php echo $mata_pelajaran ?>"/>
     <div class="control-group">
      <label class="control-label" for="focusedInput">Kategori Soal</label>
      <div class="controls">
       <input class="input-xlarge focused required" id="kategori" type="text" value="<?php echo isset($kategori) ? $kategori : '' ?>" 
              placeholder="Kategori" error="Kategori">
      </div>
     </div>
     <div class="form-actions">
      <button type="button" class="btn btn-primary" onclick="membuat_soal_data.simpanKategori()">Simpan</button>
     </div>     
    </div>    
   </form>
  </div>
 </div>
</div>